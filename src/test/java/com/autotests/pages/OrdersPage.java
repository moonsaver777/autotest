package com.autotests.pages;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class OrdersPage {
    public WebDriver driver;
    
    public OrdersPage(WebDriver driver){
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }
    @FindBy(css = "a[class*='button']")
    public WebElement linkOrderFast;

    @FindBy(xpath = "//input[@aria-label='promocode']")
    public WebElement inputPromo;

    @FindBy(xpath = "//button[@class='promo__button ']")
    public WebElement buttonPromo;

    @FindBy(xpath = "//button[@data-event='5']")
    public WebElement buttonOrderDelete;

    @FindBy(xpath = "//*[@id='js-order-clear']")
    public WebElement buttonOrderClear;

    @FindBy(xpath = "//*[@id='fastregistrationform-email']")
    public WebElement inputFastRegEmail;

    @FindBy(xpath = "//*[@id='fastregistrationform-second']")
    public WebElement inputFastRegSurname;

    @FindBy(xpath = "//*[@id='fastregistrationform-first']")
    public WebElement inputFastRegName;

    @FindBy(xpath = "//*[@id='fastregistrationform-phone']")
    public WebElement inputFastRegPhone;

    @FindBy(css = "div[class='form-group i-form-field form-group_wide form-group_agree text-left text-start field-fastregistrationform-agree required'] span[class$='checkbox__custom']")
    public WebElement checkboxFastRegAgree;

    @FindBy(xpath = "//button[.//span[@class='js-summ-fast']]")
    public WebElement buttonPay;


    public void payOrder(){linkOrderFast.click();}
    public void confPromo(){inputPromo.sendKeys("testoviy");}
    public void clickButtonPromo(){buttonPromo.click();}
    public void deleteOrder(){buttonOrderDelete.click();}
    public void clearOrder(){buttonOrderClear.click();}
    public void putEmailFastBuy(){inputFastRegEmail.sendKeys("autotests.infomatika@gmail.com");}
    public void putSurnameFastBuy(){ inputFastRegSurname.sendKeys("Test");}
    public void putNameFastBuy(){inputFastRegName.sendKeys("Test");}
    public void putPhoneFastBuy(){inputFastRegPhone.sendKeys("77777777777");}
    public void fastRegAgree(){checkboxFastRegAgree.click();}
    public void goToPay(){buttonPay.click();}

}
