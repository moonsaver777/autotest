package com.autotests.pages;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class UnitellerPage {
    public WebDriver driver;
    public UnitellerPage(WebDriver driver){
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(xpath = "//*[@id='Pan']")
    public WebElement inputCard;

    @FindBy(xpath = "//*[@id='ExpMonth']")
    public WebElement inputMonth;

    @FindBy(xpath = "//*[@id='CardholderName']")
    public WebElement inputCardName;

    @FindBy(xpath = "//*[@id='ExpYear']")
    public WebElement inputYear;

    @FindBy(xpath = "//*[@id='Cvc2']")
    public WebElement inputCvc;

    @FindBy(xpath = "//*[@id='btnPay']")
    public WebElement uniPay;

    public void inputCardNumber(){inputCard.sendKeys("4000 0000 0000 2487");}
    public void inputNameCard(){inputCardName.sendKeys("UNITELLER TEST");}
    public void inputCardMonth(){inputMonth.sendKeys("01");}
    public void inputCardYear(){inputYear.sendKeys("23");}
    public void inputCardCvc(){inputCvc.sendKeys("123");}
    public void unitellerPay(){uniPay.click();}
}
