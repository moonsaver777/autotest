package com.autotests.pages;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;
import java.util.Random;

public class SectorPage {

    public WebDriver driver;

    public SectorPage(WebDriver driver){
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    //////////////////LOCATORS\\\\\\\\\\\\\\\\\\\\\\\\\\
    @FindBy(css = "button[class='btn js-price-id-alert-btn']")
    public WebElement buttonMessage;

    @FindBy(css = "button[id='js-plan-zoom-out']")
    public WebElement buttonPlanZoomOut;

    @FindBy(css = "#svg > g[price]")
    public WebElement svgSector;

    @FindBy (css = "#svg > g[price]")
    public WebElement svgEmptySector;

    @FindBy (css = "#svg > g.active")
    public WebElement svgPlace;

    @FindBy (css = "#svg > g.active")
    public WebElement svgEmptyPlace;

    @FindBy(xpath = "//*[@id='js-places-buy']")
    public WebElement btnPlacesBuy;

    @FindBy(xpath = "//a[contains(@class, 'btn')]")
    public WebElement btnBuyOrder;
    

    /////////////////METHODS\\\\\\\\\\\\\\\\\\\\\\
    public void clickMessage(){buttonMessage.click();}
    public void svgZoomOut(){buttonPlanZoomOut.click();}
    public void clickSector(){
        List<WebElement> sectorList = driver.findElements(By.cssSelector("#svg > g[price]"));
        int maxSectors = sectorList.size();
        Random random = new Random();
        int randomSector = random.nextInt(maxSectors);
        sectorList.get(randomSector).click();
    }
    public void clickPlace(){
        List<WebElement>placesList = driver.findElements(By.cssSelector("#svg > g.active"));
        int maxPlaces = placesList.size();
        Random random = new Random();
        int randomPlace = random.nextInt(maxPlaces);
        placesList.get(randomPlace).click();
    }
    public void clickBuyPlaces(){btnPlacesBuy.click();}
    public void clickBuyTickets(){btnBuyOrder.click();}
}
