package com.autotests.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import java.util.Random;
import java.lang.String;

public class MainPage {

    int eventId = 104;
    public WebDriver driver;

    public MainPage(WebDriver driver){
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    public String emailFeed(){
        int email = Math.abs(new Random().nextInt()%9999+1);
        return email + "test@test.com";
    }


    ////////////////////HEADER LOCATORS\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    @FindBy(css = "a[class='logo__link']")
    public WebElement linkLogo;

    @FindBy(css = "a[class$='active']")
    public WebElement linkMenuActive;

    @FindBy(css = "a[title='Абонементы']")
    public WebElement linkMenu;
    @FindBy (css = "a[title='Как купить?']")
    public WebElement howToBuyLink;

    @FindBy (css = "a[title='Вопросы']")
    public WebElement questionsLink;

    @FindBy(css = "a[title='Правила покупки']")
    public WebElement buyRules;

    @FindBy(css = "#dropdown__btn--1")
    public WebElement buttonLanguageDropdown;

    /////////////////////FOOTER LOCATORS\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

    @FindBy(css = "button[class*='accept-button']")
    public WebElement buttonCookie;

    /////////////////////LOCATORS FOR BUY\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    @FindBy(css = "a[data-event-id='3']")
    public WebElement btnEvents;


    ////////////////////REGISTRATION LOCATORS\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    @FindBy(xpath = "//button[contains(@class, 'header__btn_register')]")
    public WebElement buttonRegisterModal;

    @FindBy(xpath = "//*[@id='registrationform-email']")
    public WebElement inputEmail;

    @FindBy(xpath = "//*[@id='registrationform-password']")
    public WebElement inputRegPassword;

    @FindBy(xpath = "//*[@id='registrationform-second']")
    public WebElement inputRegSurname;

    @FindBy(xpath = "//*[@id='registrationform-first']")
    public WebElement inputRegName;

    @FindBy(xpath = "//*[@id='registrationform-phone']")
    public WebElement inputRegPhone;

    @FindBy(css = "label[class$='register'] span[class$='custom']")
    public WebElement spanCheckboxCustom;

    @FindBy(css = "div[class='enter__middle mb-3'] button[class*='button']")
    public WebElement buttonEnter;


    //////////////////////////LOGIN LOCATORS\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    @FindBy(xpath = "//button[contains(@class, 'header__btn_login')]")
    public WebElement buttonLoginModal;

    @FindBy(xpath = "//*[@id='login-form-login']")
    public WebElement inputLoginName;

    @FindBy(xpath = "//*[@id='login-form-password']")
    public WebElement inputLoginFormPassword;

    @FindBy(xpath = "//button[contains(@class, 'auth_btn')]")
    public WebElement buttonModalLogin;

    @FindBy(xpath = "//button[contains(@class, 'dropdown__link')]")
    public WebElement buttonExitReset;

    @FindBy(css = "svg[class$='link-down']")
    public WebElement svgIconHeader;

    @FindBy(css = "div[class$='has-error'] div[class='help-block']")
    public WebElement emailError;

    @FindBy(xpath = "/html/body/div[4]/div/div/button")
    public WebElement buttonExitReg;


    //////////////////////HEADER METHODS\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    public void clickLogo(){linkLogo.click();}
    public void clickActive(){linkMenuActive.click();}
    public void clickAbon(){linkMenu.click();}
    public void clickHowToBuy(){howToBuyLink.click();}
    public void clickQuestions(){questionsLink.click();}
    public void clickBuyRules(){buyRules.click();}
    public void changeLanguage(){buttonLanguageDropdown.click();}

    //////////////////////FOOTER METHODS\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    public void clickCookie(){buttonCookie.click();}

    //////////////////////REGISTRATION METHODS\\\\\\\\\\\\\\\\\\\\\\\

    public void clickReg(){buttonRegisterModal.click();}

    public void putEmailPositive(){
        inputEmail.sendKeys(emailFeed());
    }

    public void putEmailNegative(){inputEmail.sendKeys("test@test.ru");}

    public void putPassword(){
        inputRegPassword.sendKeys("qwerty1234");
    }

    public void putPhone(){
        inputRegPhone.sendKeys("77777777777");
    }

    public void putName(){
        inputRegName.sendKeys("test");
    }

    public void putSurname(){
        inputRegSurname.sendKeys("testov");
    }

    public void clickCheckBox(){
        spanCheckboxCustom.click();
    }

    public void confRegistration(){
        buttonEnter.click();
    }

    public void exitRegistration(){buttonExitReg.click();}


    //////////////////////////LOGIN METHODS\\\\\\\\\\\\\\\\\\\\\\\\\
    public void clickLogin(){
        buttonLoginModal.click();
    }
    public void putLoginNamePositive(){
        inputLoginName.sendKeys("autotests.infomatika@gmail.com");
    }
    public void putLoginNameNegative(){inputLoginName.sendKeys(emailFeed());}
    public void putLoginPassword(){inputLoginFormPassword.sendKeys("Y45hj23!nmderv");}
    public void confLogin(){
        buttonModalLogin.click();
    }
    public void dropDown(){
        svgIconHeader.click();
    }
    public void logOut(){
        buttonExitReset.click();
    }

    /////////////////////////BUY TICKET METHODS\\\\\\\\\\\\\\\\\\\\\\\\\\
    public void clickBuyEvent(){btnEvents.click();}
}

