package com.autotests.tests;

import com.autotests.pages.MainPage;
import com.autotests.pages.OrdersPage;
import com.autotests.pages.SectorPage;
import com.autotests.pages.UnitellerPage;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class E2eTests {

    public static MainPage mainPage;
    public static SectorPage sectorPage;
    public static OrdersPage ordersPage;
    public static WebDriver driver;
    public static UnitellerPage unitellerPage;

    @BeforeAll
    public static void setUp()  {
        FirefoxOptions options = new FirefoxOptions();
        options.addArguments("--window-size=1920,1080");
        options.addArguments("--start-maximized");
        //options.addArguments("--headless");
        driver = new FirefoxDriver(options);
        mainPage = new MainPage(driver);
        sectorPage = new SectorPage(driver);
        ordersPage = new OrdersPage(driver);
        unitellerPage = new UnitellerPage(driver);
        driver.manage().window().maximize();
        driver.manage().deleteAllCookies();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get(ConfProperties.getProperty("mainPage"));
    }

    @AfterAll
    public static void tearDown(){
        driver.close();
    }

    @Feature("Registration")
    @Severity(SeverityLevel.CRITICAL)
    @Test
    public void RegistrationTestNegative() throws InterruptedException{
        mainPage.clickReg();
        Thread.sleep(5000);
        mainPage.putEmailNegative();
        mainPage.putPassword();
        mainPage.putPhone();
        mainPage.putName();
        mainPage.putSurname();
        mainPage.clickCheckBox();
        Thread.sleep(5000);
        mainPage.emailError.isDisplayed();
        mainPage.buttonRegisterModal.isEnabled();
        mainPage.exitRegistration();
    }
    @Feature("Registration")
    @Severity(SeverityLevel.CRITICAL)
    @Test
    public void RegistrationTestPositive() throws InterruptedException{
        mainPage.clickReg();
        Thread.sleep(5000);
        mainPage.putEmailPositive();
        mainPage.putPassword();
        mainPage.putPhone();
        mainPage.putName();
        mainPage.putSurname();
        mainPage.clickCheckBox();
        Thread.sleep(5000);
        mainPage.confRegistration();

    }
    @Feature("Login")
    @Severity(SeverityLevel.CRITICAL)
    @Test
    public void LoginTest() throws InterruptedException{
        mainPage.clickLogin();
        Thread.sleep(5000);
        mainPage.putLoginNamePositive();
        mainPage.putLoginPassword();
        Thread.sleep(2000);
        mainPage.confLogin();
        Thread.sleep(2000);
        mainPage.dropDown();
        mainPage.logOut();
    }
    @Feature("Login")
    @Severity(SeverityLevel.CRITICAL)
    @Test
    public void LoginTestNegative() throws InterruptedException{
//        mainPage.clickLogo();
        driver.get(ConfProperties.getProperty("mainPage"));
        Thread.sleep(2000);
        mainPage.clickLogin();
        Thread.sleep(5000);
        mainPage.putLoginNameNegative();
        mainPage.putLoginPassword();
        Thread.sleep(2000);
        mainPage.confLogin();
    }
    @Feature("Information Pages")
    @Severity(SeverityLevel.MINOR)
    @Test
    public void HeaderTest() throws InterruptedException{
        String MainPageTitle = "Игровой сайт | Главная";
        mainPage.clickLogo();
        Thread.sleep(2000);
        assertEquals(driver.getTitle(),MainPageTitle,"Главная страница не открывается");
    }
    @Feature("Information Pages")
    @Severity(SeverityLevel.MINOR)
    @Test
    public void HowToBuyTest() throws InterruptedException{
        String HowToBuyTitle = "Как купить?";
        mainPage.clickHowToBuy();
        Thread.sleep(2000);
        assertEquals(driver.getTitle(),HowToBuyTitle, "Страница Как Купить не открывается");
    }
    @Feature("Information Pages")
    @Severity(SeverityLevel.MINOR)
    @Test
    public void QuestionsTest() throws InterruptedException{
        String QuestionsTitle = "Вопросы";
        mainPage.clickQuestions();
        Thread.sleep(2000);
        assertEquals(driver.getTitle(),QuestionsTitle,"Страница Вопросы не открывается");
    }
    @Feature("Information Pages")
    @Severity(SeverityLevel.MINOR)
    @Test
    public void RulesTest() throws InterruptedException{
        String RulesTitle = "Правила покупки";
        mainPage.clickBuyRules();
        Thread.sleep(2000);
        assertEquals(driver.getTitle(),RulesTitle,"Страница Правила покупки не открывается");
    }
    @Feature("Tickets Buy")
    @Severity(SeverityLevel.CRITICAL)
    @Test
    public void BuyTicketFastRegistration() throws InterruptedException{
        JavascriptExecutor js = (JavascriptExecutor) driver;
        mainPage.clickLogo();

        if (mainPage.buttonCookie.isDisplayed()) {
            mainPage.clickCookie();
        }

        Thread.sleep(3000);
        js.executeScript("window.scrollBy(0,250)");
        Thread.sleep(3000);
        mainPage.clickBuyEvent();
        Thread.sleep(2000);

//        if (sectorPage.buttonMessage.isDisplayed()) {
//            sectorPage.clickMessage();
//        }

        Thread.sleep(5000);
        js.executeScript("window.scrollBy(0,850)");
//        Thread.sleep(2000);
        sectorPage.svgZoomOut();
        Thread.sleep(5000);
        sectorPage.clickSector();
        Thread.sleep(3000);
        sectorPage.clickPlace();
        Thread.sleep(3000);
        sectorPage.clickBuyPlaces();
        Thread.sleep(3000);
        ordersPage.payOrder();
        Thread.sleep(3000);
        ordersPage.putEmailFastBuy();
        ordersPage.putSurnameFastBuy();
        ordersPage.putNameFastBuy();
        ordersPage.putPhoneFastBuy();
        Thread.sleep(3000);
        ordersPage.fastRegAgree();
        Thread.sleep(3000);
        ordersPage.goToPay();
        Thread.sleep(3000);
        unitellerPage.inputCardNumber();
        unitellerPage.inputNameCard();
        unitellerPage.inputCardMonth();
        unitellerPage.inputCardYear();
        unitellerPage.inputCardCvc();
        unitellerPage.unitellerPay();
        Thread.sleep(10000);
    }
    @Feature("Tickets Buy")
    @Severity(SeverityLevel.CRITICAL)
    @Test
    public void BuyTicketNormalBuy() throws InterruptedException {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        mainPage.clickLogo();
        Thread.sleep(3000);
        mainPage.clickLogin();
        Thread.sleep(5000);
        mainPage.putLoginNamePositive();
        mainPage.putLoginPassword();
        Thread.sleep(2000);
        mainPage.confLogin();
        Thread.sleep(3000);
        js.executeScript("window.scrollBy(0,355)");
        mainPage.clickBuyEvent();
        Thread.sleep(2000);
        Thread.sleep(5000);
        js.executeScript("window.scrollBy(0,850)");
        Thread.sleep(2000);
        sectorPage.svgZoomOut();
        Thread.sleep(5000);
        sectorPage.clickSector();
        Thread.sleep(3000);
        sectorPage.clickPlace();
        Thread.sleep(3000);
        sectorPage.clickBuyPlaces();
        Thread.sleep(3000);
        ordersPage.payOrder();
        Thread.sleep(5000);
        unitellerPage.inputCardNumber();
        Thread.sleep(1500);
        unitellerPage.inputNameCard();
        Thread.sleep(1500);
        unitellerPage.inputCardMonth();
        Thread.sleep(1500);
        unitellerPage.inputCardYear();
        Thread.sleep(1500);
        unitellerPage.inputCardCvc();
        Thread.sleep(1500);
        unitellerPage.unitellerPay();
    }
}